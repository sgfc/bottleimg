#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
from project import app
from bottle import debug, run

if __name__ == '__main__':
    port = int(sys.argv[1])
    try:
        if sys.argv[2] == 'dev':
            debug(True)
            run(app, reloader=True, host='0.0.0.0', port=port, server='bjoern')
    except:
        debug(False)
        run(app, reloader=False, host='0.0.0.0', port=port, server='rocket')
