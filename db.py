import cymysql
from web import db


class DB(object):

    conn = None
    cur = None
    retries = 3

    def __init__(self, host='localhost', user='root', password='', database=''):
        self.host = host
        self.user = user
        self.password = password
        self.database = database

        self._init_connection()

    def _init_connection(self):
        if self.retries < 1:
            raise DBConnectionFailed()
        try:
            self.conn = cymysql.connect(host=self.host, user=self.user,
                                        passwd=self.password, db=self.database,
                                        connect_timeout=604800, charset='utf8',
                                        use_unicode=True)
            self.cur = self.conn.cursor(cymysql.cursors.DictCursor)
        except:
            self.retries -= 1
            print "WARNING: MySQL connection died, trying to reinit..."
            self._init_connection()

    def escape(self, obj):
        try:
            if isinstance(obj, str):
                return self.conn.escape(obj)
            else:
                return obj
        except cymysql.OperationalError:
            print "WARNING: MySQL connection died, trying to reinit..."
            self._init_connection()
            return self.escape(obj)

    def execute(self, sql, args=None):
        try:
            _sql = sql
            sql = _sql.query()
            args = tuple(_sql.values())
            print sql, args
            if args is not None:
                self.cur.execute(sql, args)
            else:
                self.cur.execute(sql)
            return self.cur
        except cymysql.OperationalError:
            print "WARNING: MySQL connection died, trying to reinit..."
            self._init_connection()
            return self.execute(sql, args)

    def fetchone(self, sql, args=None):
        cur = self.execute(sql, args)
        if cur.rowcount:
            return cur.fetchone()
        else:
            return None

    def fetchall(self, sql, args=None):
        cur = self.execute(sql, args)
        if cur.rowcount:
            return cur.fetchall()
        else:
            return None

    def query(self, query, vars=None):
        if vars is not None:
            query = db.reparam(query, vars)
        return self.fetchall(query)


class DBConnectionFailed(Exception):

    ''' happens when a database operation continually fails '''
