Dependencies
------------

you need to get the latest bottle from https://github.com/defnull/bottle

    pip install web.py bjoern mako pillow python-magic tempita markdown2 pygments

(we use web.py for it's excellent database support)

To run:

    python runserver.py <port>


Take a look at ```project/config.example.py```, then configure it and rename it to ```project/config.py```

LICENSE
-------

        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

    Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

    Everyone is permitted to copy and distribute verbatim or modified
    copies of this license document, and changing it is allowed as long
    as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

    0. You just DO WHAT THE FUCK YOU WANT TO.