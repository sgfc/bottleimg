from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Frame, Table
from reportlab.lib.units import mm
from reportlab.lib.colors import black
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.pdfbase.ttfonts import TTFont
import subprocess
import xml.etree.ElementTree as etree
from urllib import urlopen
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import time


@app.route('/lastfm/<name>')
def lastfm(name):

    def getXML(benis):
        xml = urlopen("http://normalisr.com/?username=" +
                      benis + "&chart=artist&type=overall&format=xml2")
        local_file = open(
            config.Settings['directories']['thumbs'] + benis + '.xml', "w")
        local_file.write(xml.read())
        local_file.close()
        return config.Settings['directories']['thumbs'] + benis + '.xml'

    os.chdir(config.Settings['directories']['thumbs'])
    if os.path.isfile(name + '.pdf') and os.path.isfile(name + '.png'):
        os.remove(name + '.pdf')
        os.remove(name + '.png')

    xmlname = config.Settings['directories']['thumbs'] + name + '.xml'

    if os.path.isfile(name + '.xml'):
        if os.stat(xmlname).st_mtime < time.time() - 86400 / 2 or request.query.freshen:
            xml = getXML(name)
        else:
            xml = xmlname
    else:
        xml = getXML(name)

    canvas = Canvas(name + '.pdf', pagesize=(232.5, 410))
    frame = Frame(0, 0, 232.5, 410, showBoundary=1)
    pdfmetrics.registerFont(TTFont('benis', 'ipaexg.ttf'))
    pdfmetrics.registerFont(TTFont('Vera', 'Vera.ttf'))

    canvas.drawString(100, 750, "Welcome to Reportlab!")

    times = time.strftime("%a, %d %b %Y %H:%M:%S +10:00",
                          time.localtime(os.stat(xmlname).st_mtime))

    table_data = [['My top artists overall', '', '']]

    print xml
    tree = etree.parse(xml)
    root = tree.getroot()
    rows = []
    for child in root:
        for ch in child:
            if int(ch.attrib['normalised']) <= 20:
                artist = ch[0].text.encode('utf-8')
                clipamount = 200
                while stringWidth(artist, 'benis', 10) > 130:
                    artist = artist[:clipamount] + '...'
                    clipamount -= 1
                    # print artist, clipamount, stringWidth(artist,'benis',10)
                print artist, clipamount, stringWidth(artist, 'benis', 10)
                # else:
                #     artist = ch[0].text.encode('utf-8')
                table_data.append(
                    [ch.attrib['normalised'], artist, str(round(float(ch[11].text) / 60, 1)) + ' hours'])

    table_data.append(['Last updated: ' + times, '', ''])

    table_style = [
        ("FONTNAME", (0, 0), (2, 21), 'benis'),
        ("FONTSIZE", (0, 0), (2, 20), 10),
        ("FONTSIZE", (0, -1), (2, -1), 8),
        ('GRID', (0, 0), (2, 20), 1, black),
        ('SPAN', (0, 0), (2, 0)),
        ('SPAN', (0, -1), (2, -1)),
        ('ALIGN', (2, 1), (2, 20), 'RIGHT'),
        ('ALIGN', (0, 0), (2, 0), 'CENTER'),
        ('ALIGN', (0, 1), (0, 20), 'CENTER'),
        ('ALIGN', (0, -1), (2, -1), 'CENTER')
    ]

    table = Table(table_data, (20, 140, 70), style=table_style)
    frame.add(table, canvas)
    canvas.save()

    if request.query.transparent:
        subprocess.check_output('cd ' + config.Settings['directories']['thumbs'] +
                                '; gs -q -dNOPAUSE -dBATCH -sDEVICE=pngalpha -r80 -dEPSCrop -sOutputFile="%s.png" "%s.pdf"' % (name, name), shell=True)
    else:
        subprocess.check_output('cd ' + config.Settings['directories']['thumbs'] +
                                '; convert -density 80 "%s[0]" -trim "%s.png"' % (name + '.pdf', name), shell=True)
    return static_file(name + '.png', root=config.Settings['directories']['thumbs'])
