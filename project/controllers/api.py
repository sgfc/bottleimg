# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
from mimetypes import guess_extension
from cStringIO import StringIO
import shutil
from math import ceil
import urllib
import datetime
from tempita import looper
import hashlib
from PIL import Image, ImageOps
from wsgiref.handlers import format_date_time
from time import mktime
import pprint
from datetime import datetime

db = config.db
Settings = config.Settings


def modification_date(filename):
    t = utils.mtime(filename)
    return datetime.fromtimestamp(t).isoformat()


@app.route('/api/file/<url>')
def GET(url):
    if url == "":
        return "This file doesn't exist."
    else:
        if url.split('.')[-1]:
            url = url.split('.')[0]
        results = db.query(
            'SELECT * FROM `keys` WHERE `shorturl`  = ' + config.dbn.sqlquote(url))
        if results:
            for row in results:
                _type = 'image' if row['not_image'] == 'n' or row['not_image'] == "0" else 'file' if row[
                    'is_paste'] == 'n' or row['is_paste'] == '0' else 'paste'
                _type = 'paste' if row['is_paste'] == 'y' else _type
                orig = row['original'] if row['original'] else row['url']
                response.content_type = 'application/json; charset=utf-8'
                if _type == 'image' or _type == 'file':
                    infos = {
                        "type": _type,
                        "url": 'http://' + request.environ.get('HTTP_HOST') + '/' + row['shorturl'],
                        "filename": orig,
                        "hits": row['hits'],
                        "mimetype": magic.from_file(Settings['directories']['private'] + row['url'], mime=True),
                        "size": utils.fsize(Settings['directories']['private'] + row['url']),
                        "date": modification_date(Settings['directories']['private'] + row['url'])
                    }
                    if _type == 'image':
                        res = Image.open(
                            Settings['directories']['private'] + row['url']).size
                        infos["resolution"] = {
                            "width": res[0],
                            "height": res[1]
                        }
                    return json.dumps(infos)
                else:
                    return json.dumps({
                        "type": _type,
                        "name": row['url'],
                        "url": 'http://' + request.environ.get('HTTP_HOST') + '/paste/' + row['shorturl'],
                        "content": orig,
                        "hits": row['hits']
                    })
        else:
            return "This file doesn't exist."
