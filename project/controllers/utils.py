import string
import sys
import urllib
import os
from random import choice
import re
from tempita import looper
import pygments
from pygments.util import ClassNotFound
from pygments.lexers import get_lexer_by_name, get_lexer_for_filename, \
    get_lexer_for_mimetype, PhpLexer, TextLexer, get_all_lexers, \
    guess_lexer, guess_lexer_for_filename
from pygments.styles import get_all_styles
from pygments.formatters import HtmlFormatter
import pprint
from project import app, config
import bottle
import datetime
import base64
import uuid
import hashlib

sortmodes = (
    ('date_desc', 'Date (descending)'),
    ('date_asc', 'Date (ascending)'),
    ('only_pastes', 'Only Pastes'),
    ('only_images', 'Only Images'),
    ('only_files', 'Only Files'),
    ('hits_desc', 'Hits (descending)'),
    ('hits_asc', 'Hits (ascending)')
)

sortmode = []
for e in sortmodes:
    sortmode.append(e[0])


class AttrDict(dict):

    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def maxSize(image, maxSize, method=3):
    imAspect = float(image.size[0]) / float(image.size[1])
    outAspect = float(maxSize[0]) / float(maxSize[1])

    if imAspect >= outAspect:
        return image.resize((maxSize[0], int((float(maxSize[0]) / imAspect) + 0.5)), method)
    else:
        return image.resize((int((float(maxSize[1]) * imAspect) + 0.5), maxSize[1]), method)


def prettify(x):
    try:
        out = pprint.pformat(x)
    except Exception, e:
        out = '[could not display: <' + e.__class__.__name__ + \
              ': ' + str(e) + '>]'
    return out


def linkExists(url):
    exists = urllib.urlopen(url).getcode()
    if exists:
        if exists == 200 or exists == 301 or exists == 302:
            return True
        else:
            return False
    else:
        return False


def get_cookies(*args):
    cookies = {}
    for e in args:
        if bottle.request.get_cookie(e):
            _stri = bottle.request.get_cookie(e)
        else:
            _stri = ''.join(choice(string.letters) for i in xrange(15))
            bottle.response.set_cookie(
                e, _stri, max_age=config.Settings['cookies']['expire']
            )
        cookies[e] = _stri
    return cookies


def set_cookies(*args):
    for e in args:
        try:
            bottle.response.set_cookie(
                e[0], e[1], max_age=e[2]
            )
            print "set cookie", e[0], e[1]
        except:
            bottle.response.set_cookie(
                e[0], e[1], max_age=config.Settings['cookies']['expire']
            )


def escape(s, quote=False):
    if s is None:
        return ''
    elif hasattr(s, '__html__'):
        return s.__html__()
    elif not isinstance(s, basestring):
        s = unicode(s)
    s = s.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')
    if quote:
        s = s.replace('"', "&quot;")
    return s


def mtime(filename):
    try:
        return os.stat(filename).st_mtime
    except:
        return None


def modification_date(filename):
    t = mtime(filename)
    return datetime.datetime.fromtimestamp(t).strftime('%d/%m/%Y @ %H:%M:%S')


def fsize(filename):
    try:
        return os.stat(filename).st_size
    except:
        return None

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

# following from Python cookbook, #475186


def has_colours(stream):
    if not hasattr(stream, "isatty"):
        return False
    if not stream.isatty():
        return False  # auto color only on TTYs
    try:
        import curses
        curses.setupterm()
        return curses.tigetnum("colors") > 2
    except:
        # guess false in case of error
        return False
has_colours = has_colours(sys.stdout)


def printout(text, colour=WHITE):
        if has_colours:
                seq = "\x1b[1;%dm" % (30 + colour) + text + "\x1b[0m"
                sys.stdout.write(seq)
        else:
                sys.stdout.write(text)


def do_print(text, colour=GREEN, status=None):
    sys.stdout.write('[')
    status = status or " ok "
    printout(status, colour)
    sys.stdout.write('] ')
    print text


def sizeof_fmt(num, short=None):
        for x in ['bytes', 'KB', 'MB', 'GB']:
            if num < 1024.0 and num > -1024.0:
                return "%3.2f %s" % (num, x) if not short else "%3.0f %s" % (num, x)
            num /= 1024.0
        return "%3.1f %s" % (num, 'TB')


def hash_password(password, salt=None):
    if salt is None:
        salt = uuid.uuid4().hex

    hashed_password = hashlib.sha512(password + salt).hexdigest()

    return (hashed_password, salt)


def verify_password(password, hashed_password, salt):
    re_hashed, salt = hash_password(password, salt)

    return re_hashed == hashed_password

do_print("utils.py loaded")
