# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
from mimetypes import guess_extension
from cStringIO import StringIO
import shutil
from math import ceil
import urllib
import datetime
from tempita import looper
import hashlib
from PIL import Image, ImageOps
from wsgiref.handlers import format_date_time
from time import mktime
import pprint
from pygments.util import ClassNotFound
from pygments.lexers import get_lexer_by_name, get_lexer_for_filename, \
     get_lexer_for_mimetype, PhpLexer, TextLexer, get_all_lexers, \
     guess_lexer, guess_lexer_for_filename
from pygments.styles import get_all_styles
from pygments.formatters import HtmlFormatter

db = config.db
Settings = config.Settings
_cookies = get_cookies(Settings['cookies']['key'], Settings['cookies']['password'])

do_print("pastes.py loaded")

LANGUAGES = get_all_lexers()
_section_marker_re = re.compile(r'^(?<!\\)###\s*(.*?)(?:\[(.+?)\])?\s*$(?m)')
_escaped_marker = re.compile(r'^\\(?=###)(?m)')

def get_language_for(filename, mimetype=None, default='text'):
    """Get language for filename and mimetype"""
    try:
        if mimetype is None:
            raise ClassNotFound()
        lexer = get_lexer_for_mimetype(mimetype)
    except ClassNotFound:
        try:
            lexer = get_lexer_for_filename(filename)
        except ClassNotFound:
            return default
    return get_known_alias(lexer, default)

def get_language_for_code(code, mimetype=None, default='text'):
    """Get language for filename and mimetype"""
    try:
        lexer = guess_lexer(code)
    except ClassNotFound:
        return default
    return get_known_alias(lexer, default)


def lookup_language_alias(alias, default='text'):
    """When passed a pygments alias returns the alias from LANGUAGES. If
the alias does not exist at all or is not in languages, `default` is
returned.
"""
    if alias in LANGUAGES:
        return alias
    try:
        lexer = get_lexer_by_name(alias)
    except ClassNotFound:
        return default
    return get_known_alias(lexer)


def get_known_alias(lexer, default='text'):
    """Return the known alias for the lexer."""
    for alias in lexer.aliases:
        if alias in LANGUAGES:
            return alias
    return default


def list_languages():
    """List all languages."""
    languages = LANGUAGES.items()
    languages.sort(key=lambda x: x[1].lstrip(' _-.').lower())
    return languages

sections = []

def highlight_multifile(code):
    """Multi-file highlighting."""
    result = []

    global sections

    last = [0, None, 'text']

    def highlight_section(pos, indx):
        start, filename, lang = last
        section_code = _escaped_marker.sub('', code[start:pos])
        if section_code:
            result.append(u'<div class="section" style="margin-top:10px" id="'+ str(indx - 1) +'">%s%s</div>' % (
                filename and u'<h2 class="filename">%s <a href="#container" style="float:right">[top]</a></h2>'
                    % escape(filename + '[' + lang + ']') or u'',
                highlight(section_code, lang)
            ))

    sections = []
    for loop, i in looper(_section_marker_re.finditer(code)):
        start = loop.item.start()
        #lang = get_language_for_code(start)
        filename, lang = loop.item.groups()
        sections.append([loop.index, loop.item.groups()[0]])
        #new_idx = idx + 1 if len(list(enumerate(_section_marker_re.finditer(code)))) == idx else idx
        highlight_section(start, loop.index)
        lang = loop.item.groups()[1]
        last = [loop.item.end(), filename, lang]
        if len(sections) > len(list(enumerate(_section_marker_re.finditer(code)))):
            sections = sections[:len(list(enumerate(_section_marker_re.finditer(code))))]

    highlight_section(len(code),len(list(enumerate(_section_marker_re.finditer(code)))))

    return u'<div class="multi">%s</div>' % u'\n'.join(result)


def highlight(code, language, _preview=False, _linenos=True):
    if language == 'php':
        lexer = PhpLexer(startinline=True)
    elif language == 'guess_pls':
        lexer = guess_lexer(code)
    elif language == 'multi':
        return highlight_multifile(code)
    else:
        try:
            lexer = get_lexer_by_name(language)
        except ClassNotFound:
            lexer = TextLexer()
    formatter = HtmlFormatter(linenos=_linenos, cssclass='syntax', style='github')
    return u'<div class="syntax">%s</div>' % \
           pygments.highlight(code, lexer, formatter)

@app.get('/paste')
@app.get('/paste/')
def paste_home():
    return template('pastebin',title='Pastebin', keyc=request.get_cookie('key'), passc=request.get_cookie('key_pass'))

@app.get('/paste/<keys>')
@app.get('/paste/<keys>/<flag>')
def paste_view(keys, flag=None):
    i = AttrDict(
        edit=int(request.query.get('edit', default=0)),
    )
    i.edit = int(i.edit)

    view_key = keys
    global sections

    if view_key:
        results = db.query('SELECT * FROM `keys` WHERE `shorturl` = $url', vars={'url': view_key})
        text = False
        if results:
            for row in results:
                title = row['url']
                db.query('UPDATE `keys` SET hits = hits + 1 WHERE `shorturl` = $url', vars={'url': view_key})
                orig = row['original'] if row['original'] else 'No paste data! (how did that happen)'
                text = True
                hits = row['hits']
        else:
            abort(404)
        if not text:
            abort(404)

    if row['url'] != row['shorturl']:
        title = 'Paste ' + row['url'] + ' (' + row['shorturl'] + ')'
    else:
        title = 'Paste ' + row['url']

    if flag == 'raw':
        response.content_type = 'text/plain; charset=utf-8'
        return orig
    else:
        return template('paste',
            title=title,
            content=[
                len(orig),
                len(orig.split('\n')),
                highlight(orig, row['paste_lang']),
                hits
            ],
            css=HtmlFormatter(style='github').get_style_defs('.syntax'),
            sect=sections if row['paste_lang'] == 'multi' else False,
            url=row['shorturl'],
            edit=i.edit,
            lang=row['paste_lang']
        )
        sections = []

@app.post('/upload/paste')
def pastebin_upload():
    x = AttrDict(
        key = request.forms.get('key'),
        password = request.forms.get('password'),
        paste_body = request.forms.get('paste_body', default=None),
        sharex = request.forms.get('sharex', default=0),
        paste_name = request.forms.get('paste_name'),
        lang = request.forms.get('lang', default='text'),
        _type = request.forms.get('type')
    )

    pprint.pprint(x)

    if re.match("^[a-zA-Z0-9_-]+$", x.key):
        random_str = ''.join(choice(string.letters) for i in xrange(5 if x._type == 'private' else 5))

        public = x._type == 'public' or False
        key = 'public' if public else x.key
        error = None
        authed = True if public else False
        password = 'public' if public else x.password

        if not x.sharex:
            if key is not "public":
                set_cookies(
                    (config.Settings['cookies']['key'], key, config.Settings['cookies']['expire']),
                    (config.Settings['cookies']['password'], password, config.Settings['cookies']['expire'])
                )


        if not public:
            check_password_query = config.db.query('SELECT `passs` FROM `keys` WHERE `key` = $key', vars={'key': key})
            if check_password_query:
                if check_password_query[0]['passs'] == password:
                    authed = True
            else:
                authed = True

        if not authed:
            error = 'Password for the key "%s" is wrong!' % key
            print 'wrong pass for key "%s" rofl xd' % key
        if 'paste_body' in x and not error:
            types = x._type
            is_paste = 1

            if x.paste_body == '' or x.paste_body is None:
                error = 'No text to paste!'

            if len(x.paste_body) > 60000:
                error = "Your paste exceeds the maximum character length of 60000! (your paste had " + str(len(x.paste_body)) + " characters)"

            if not error:
                not_image = 1

                paste_lang = x.lang

                if x.paste_name:
                    url = x.paste_name
                else:
                    url = random_str

                if not error:
                    db.query('INSERT INTO `keys` (`key`, `url`, `passs`,`shorturl`,`original`,`not_image`,`is_paste`,`paste_lang`) VALUES ($key,$url,$passs,$shorturl,$original,$notimage,$is_paste,$paste_lang)',
                        vars={'key': key, 'url': url, 'passs': password, 'shorturl': random_str, 'original': x.paste_body, 'notimage': not_image, 'is_paste': is_paste, 'paste_lang': paste_lang})

                    strl = random_str #if x.type == 'private' else random_str + ext
    else:
        error = 'sorry that key is invalid. (can only contain letters, numbers, underscores and hyphens)'

    Settings['directories']['url'] = 'http://'+request.environ.get('HTTP_HOST')

    if error:
        return template('error', error=error)
    else:
        return template('upload',baseurl=Settings['directories']['url']+'/paste', url=strl, key=key)
