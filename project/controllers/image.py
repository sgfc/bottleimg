# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
from mimetypes import guess_extension
from cStringIO import StringIO
import shutil
from math import ceil
import urllib
import datetime
from tempita import looper
import hashlib
from PIL import Image, ImageOps
from wsgiref.handlers import format_date_time
from time import mktime
import pprint


def check_image_with_pil(path):
    try:
        Image.open(path)
    except IOError:
        return False
    return True


@app.post('/gallery/<key:re:[a-zA-Z0-9_-]+>')
def auth_gallery(key):
    i = AttrDict(
        authcode=request.forms.get('authcode'),
        remember=int(request.forms.get('remember', default=1))
    )
    set_cookies((key + '+authcode', hashlib.sha1(i.authcode)
                .hexdigest(), (3600 * 24 * 7 * 30 * 12) if i.remember else None))
    redirect('/gallery/' + key)


@app.get('/gallery')
@app.get('/gallery/')
@app.get('/gallery/<key:re:[a-zA-Z0-9_-]+>')
def gallery_view(key=''):
    i = AttrDict(
        page=int(request.query.get('page', default=1)),
        sort=request.query.get('sort'),
        view=request.query.get('view', default='no'),
        query=request.query.get('query'),
        query_in=request.query.get('query_in'),
        case=int(request.query.get('case', default=0))
    )

    view = i.view
    page = i.page
    query = i.query
    qry_i = i.query_in
    case = i.case
    sort = i.sort
    sql_search = ''
    pjax = request.headers.get('X-AJAX') == 'true'
    error = False
    prev = False
    next = False
    page_error = False
    key = 'public' if not key else key
    keys = key
    public = True if key == 'public' else False
    limit = 0 if page == 1 else 30 * (page - 1)

    def load_gallery():
        print view
        view_id = 'gallery_content' if view == 1 or view == '1' else 'testgallery'
        return template('combined_gallery',
                        baseurl=config.Settings['directories']['url'],
                        files=files, error=error, public=public,
                        page=page, keyz=key, pages=last_page,
                        entries=total_entries, lel=prev, lelnext=next, sortmode=sort,
                        querys='?' + request.query_string, sortmodes=utils.sortmodes, limit=limit,
                        type=view_id, queryz=query, qryi=qry_i, case=case, pjax=pjax,
                        datetime=datetime, re=re, escape=escape, looper=looper, round=round,
                        pprint=pprint, thisk=keys
                        )

    if not public:
        settings_query = config.db.query(
            'SELECT * FROM `settings` WHERE `key` = $key', vars={'key': key})
    try:
        sqry = settings_query[0]
        if not sort:
            sort = sqry['sort_mode'] if settings_query else 'date_desc'
        else:
            sort = sort if sort in utils.sortmode else 'date_desc'
        if view == 'no':
            view = int(sqry['view_mode'])
            print view
    except:
        sort = sort if sort in utils.sortmode else 'date_desc'
        sqry = {}
        sqry['block_access'] = 0

    assign = {
        "filename": 'original',
        "url": 'shorturl',
        "content": 'original'
    }

    collate = 'COLLATE utf8_bin' if case else ''
    if qry_i == 'filename':
        s = '`original` ' + collate + ' LIKE '
        sql_search = s + config.dbn.sqlquote('%' + query + '%') + ' AND'
    elif qry_i == 'url':
        sql_search = '`shorturl` ' + collate + ' LIKE ' + \
            config.dbn.sqlquote('%' + query + '%') + ' AND'
    elif qry_i == 'content':
        sort = 'only_pastes'
        sql_search = '`original` ' + collate + ' LIKE ' + \
            config.dbn.sqlquote('%' + query + '%') + ' AND'

    print sql_search

    teq = ''
    o_teq = 'SELECT COUNT(`key`) AS total FROM `keys` WHERE ' + \
        sql_search + ' `key` = ' + config.dbn.sqlquote(key)

    if sort == 'only_images':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(
                key) + ' AND (`not_image` = "n" OR `not_image` = "0") AND (`is_paste` = "n" OR `is_paste` = "0") ORDER BY `id` DESC LIMIT ' + str(limit) + ',30'
        teq = 'SELECT COUNT(`key`) AS total FROM `keys` WHERE ' + sql_search + \
            ' (`not_image` = "n" OR `not_image` = "0") AND (`is_paste` = "n" OR `is_paste` = "0") AND `key` = ' + \
            config.dbn.sqlquote(key)
    elif sort == 'only_pastes':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(
                key) + ' AND (`is_paste` = "y" OR `is_paste` = "1") ORDER BY `id` DESC LIMIT ' + str(limit) + ',30'
        teq = 'SELECT COUNT(`key`) AS total FROM `keys` WHERE ' + sql_search + \
            ' (`is_paste` = "y" OR `is_paste` = "1") AND `key` = ' + \
            config.dbn.sqlquote(key)
    elif sort == 'only_files':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(
                key) + ' AND (`is_paste` = "n" OR `is_paste` = "0") AND (`not_image` = "y" OR `not_image` = "1") ORDER BY `id` DESC LIMIT ' + str(limit) + ',30'
        teq = 'SELECT COUNT(`key`) AS total FROM `keys` WHERE ' + sql_search + \
            ' (`is_paste` = "n" OR `is_paste` = "0") AND (`not_image` = "y" OR `not_image` = "1") AND `key` = ' + \
            config.dbn.sqlquote(key)
    elif sort == 'date_asc':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(key) + \
            ' ORDER BY `id` ASC LIMIT ' + \
            str(limit) + ',30'
    elif sort == 'hits_desc':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(key) + \
            ' ORDER BY `hits` DESC LIMIT ' + \
            str(limit) + ',30'
    elif sort == 'hits_asc':
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(key) + \
            ' ORDER BY `hits` ASC LIMIT ' + \
            str(limit) + ',30'
    else:
        qry = 'SELECT * FROM `keys` WHERE ' + sql_search + ' `key` = ' + \
            config.dbn.sqlquote(key) + \
            ' ORDER BY `id` DESC LIMIT ' + \
            str(limit) + ',30'

    total_entries_query = config.db.query(teq or o_teq)
    results = config.db.query(qry)

    total_entries = total_entries_query[0]['total']
    last_page = int(ceil(float(total_entries) / 30))
    last_page = 1 if last_page == 0 else last_page

    if page > last_page:
        error = "m8 there aren't this many pages (only " + \
            str(last_page) + " of 'em)"
        page_error = True
    else:
        next = page + 1 if page + 1 <= last_page else page + 1
        prev = page - 1 if page > 1 else page
    if results:
        grabbed = []
        for row in results:

            original_name = row['original'] or row['url']
            shorturl = row['shorturl'] or row['url']

            try:
                im = Image.open(
                    config.Settings['directories']['private'] + row['url'])
            except:
                im = None

            try:
                stats = os.stat(
                    config.Settings['directories']['private'] + row['url'])
            except:
                stats = None

            if row['not_image'] == 'n' or row['not_image'] == 0:
                try:
                    dimensions = im.size  # (height,width) tuple
                except:
                    dimensions = []
            else:
                dimensions = []

            if row['is_paste'] == 'n' or not row['is_paste']:
                if stats:
                    size = stats.st_size
                else:
                    size = []

                try:
                    mod_date = os.path.getmtime(
                        config.Settings['directories']['private'] + row['url'])
                    mod_date = datetime.datetime.fromtimestamp(
                        mod_date).strftime('%d/%m/%Y @ %H:%M:%S')
                except:
                    mod_date = False

                if row['not_image'] == 'n' or not row['not_image']:
                    try:
                        image_type = im.format
                    except:
                        try:
                            image_type = magic.from_file(
                                config.Settings['directories']['private'] + row['url'])
                        except:
                            image_type = []
                else:
                    image_type = []
            else:
                size = len(original_name)
                image_type = []
                mod_date = False

            grabbed.append([
                unicode(row['url']) + ';' + unicode(row['is_paste']) +
                ';' + unicode(row['not_image']),
                original_name,
                shorturl,
                dimensions,
                size,
                mod_date,
                image_type,
                row['hits']
            ])
        files = grabbed
    else:
        if not page_error:
            key_exists = config.db.query(o_teq)
            if key_exists:
                error = 'No search results. Try again?'
            else:
                error = 'This key was not found. (this probably means nothing has been uploaded under this key)'
        files = None

    if sqry['block_access'] == 1 and not public:
        if hashlib.sha1(sqry['password']).hexdigest() == request.cookies.get(key + '+authcode'):
            return load_gallery()
        else:
            return template(
                'general',
                title='Authenticate',
                content='You need to authenticate to view this gallery.<br/><br /> ' +
                '<form action="" method="post"><input type="password" name="authcode" placeholder="gallery password" /><br /><br />Remember this key?<br />' +
                '<input type="radio" name="remember" value="1" id="pub" checked><label>Yes</label>' +
                '<input type="radio" name="remember" value="0" id="priv"><label>No</label><br /><br />' +
                '<input type="submit" /></form>',
                css='#main {text-align:center !important}')
    else:
        return load_gallery()


@app.post('/upload')
def upload_process():
    form = AttrDict(
        key=request.forms.get('key'),
        password=request.forms.get('password'),
        _type=request.forms.get('type'),
        sharex=request.forms.get('sharex', default=0),
        json=int(request.forms.get('json', default=0)),
        uptype=request.forms.get('up_type', default='file'),
        upfile=request.files.get('files'),
        upurl=request.forms.get('urls')
    )

    error = None
    filedir = config.Settings['directories']['private']
    random = ''.join(choice(string.letters) for i in xrange(5))
    public = form._type == 'public' or False
    key = 'public' if public else form.key
    password = 'public' if public else form.password
    authed = True if public else False
    ext = ''

    form.sharex = 1 if form.sharex == 'yes' else form.sharex

    if public or re.match("^[a-zA-Z0-9_-]+$", key):

        if form.upfile or form.upurl:
            if not public:
                check_password_query = config.db.query(
                    'SELECT `passs` FROM `keys` WHERE `key` = $key', vars={'key': key})
                if check_password_query:
                    if check_password_query[0]['passs'] == form.password:
                        authed = True
                else:
                    authed = True

            if not authed:
                error = 'Password for the key "%s" is wrong!' % key
                print 'wrong pass for key "%s" rofl xd' % key
            else:
                if key is not "public":
                    set_cookies(
                        (config.Settings['cookies']['key'], key,
                         config.Settings['cookies']['expire']),
                        (config.Settings['cookies']['password'],
                         password, config.Settings['cookies']['expire'])
                    )

                if form.uptype == 'file':
                    name, ext = os.path.splitext(form.upfile.filename)
                    if '.' not in form.upfile.filename:
                        name = random
                        print name
                    if ext == '':
                        buff = form.upfile.file.read()
                        ext = guess_extension(
                            magic.from_buffer(buff, mime=True))
                    filename = name + ext
                    print name, random
                    if name == random:
                        fout = open(filedir + random + ext, 'w')
                        fout.write(buff)
                        fout.close()
                    else:
                        form.upfile.save(filedir + random + ext)
                    print 'saved to: ', config.Settings['directories']['private'] + random + ext
                else:
                    filename = form.upurl.split('/')[-1]
                    if filename.split('.')[-1] != '' and filename.split('.')[-1] != filename:
                        print 'split: ', filename.split('.')[-1]
                        ext = '.' + filename.split('.')[-1]
                    print 'opening ', form.upurl, '...'
                    f = urllib.urlopen(form.upurl)
                    resp = f.getcode()
                    error = 'Dead link supplied.'
                    if f and (resp == 200 or resp == 301 or resp == 302):
                        stringio = StringIO(f.read())
                        error = None
                        if ext == '' or ext == '.':
                            ext = guess_extension(
                                magic.from_buffer(stringio.getvalue(), mime=True))
                        print 'writing to ', filedir + random + ext, '...'
                        fout = open(filedir + random + ext, 'w')
                        fout.write(stringio.getvalue())
                        fout.close()
                        stringio.close()

            if not error:
                mime = magic.from_file(filedir + random + ext, mime=True)
                not_image = 0 if mime in [
                    "image/gif", "image/jpeg", "image/pjpeg", "image/png"] else 1
                if not_image == 1:
                    not_image = 0 if check_image_with_pil(
                        filedir + random + ext) else 1

                config.db.query(
                    'INSERT INTO `keys` (`key`, `url`, `passs`,`shorturl`,`original`,`not_image`) VALUES ($k,$u,$p,$s,$o,$n)',
                    vars={'k': key, 'u': random + ext, 'p': password, 's': random, 'o': filename, 'n': not_image})

                print 'http://' + request.environ.get('HTTP_HOST')
                config.Settings['directories']['url'] = 'http://' + \
                    request.environ.get('HTTP_HOST')

                if not form.json:
                    if form.sharex:
                        return 'URL: <a href="' + '/' + random + '">' + config.Settings['directories']['url'] + '/' + random + '</a><br/>'
                    else:
                        return template(
                            'upload',
                            key=key,
                            baseurl=config.Settings['directories']['url'],
                            url=random
                        )
                else:
                    response.content_type = 'text/html; charset=utf-8'
                    return json.dumps(
                        dict(
                            success=True if not error else False,
                            error=False if not error else error,
                            image_url='/' + random,
                            pub=True if public else False,
                            key=key,
                            base=config.Settings['directories']['url'],
                            notimage=not_image
                        )
                    )
        else:
            error = 'No file/url supplied.'
    else:
        error = 'Invalid key given. (can only contain letters, numbers, underscores and hyphens)'

    return template('error', error=error)


@app.post('/gallery/delete')
def deleter():
    x = AttrDict(
        delete_this=request.forms.getall('delete_this'),
        key=request.forms.get('key'),
        password=request.forms.get('password')
    )
    print x.key
    results = config.db.query(
        'SELECT `passs` FROM `keys` WHERE `key` = $key', vars={'key': x.key})
    error = []
    if x.key == 'public':
        error.append('i dont think so jim')
    if not error:
        if results:
            if results[0]['passs'] == x.password:
                if x.delete_this:
                    for shorturl in x.delete_this:
                        item_qry = config.db.query(
                            'SELECT * FROM `keys` WHERE `key` = $key AND `shorturl` = $shorturl AND `passs` = $passs',
                            vars={'key': x.key, 'passs': x.password, 'shorturl': shorturl})

                        if item_qry:
                            print shorturl, 'belong to', x.key
                            result = item_qry[0]
                            go_on = True
                        else:
                            error.append(
                                "This item doesn't exist, or doesn't belong to this key/password. (" + shorturl + ")")
                            print shorturl, '!belong to', x.key
                            go_on = False

                        if go_on:
                            print 'going on with', shorturl, '@', x.key
                            del_results = config.db.query(
                                'DELETE FROM `keys` WHERE `key` = $key AND `shorturl` = $shorturl AND `passs` = $passs',
                                vars={'key': x.key, 'passs': x.password, 'shorturl': shorturl})

                            if result['is_paste'] != 'y' and result['is_paste'] != '1' and result['is_paste'] != 1:
                                print result['is_paste']
                                try:
                                    actual_name = result['url']
                                    os.remove(
                                        config.Settings['directories']['private'] + result['url'])
                                except OSError:
                                    error.append(
                                        "Couldn't delete " + result['url'] + "!")
                            else:
                                actual_name = shorturl

                            if del_results:
                                error.append('Deleted ' + shorturl + '!')
                            else:
                                error.append(
                                    "Couldn't delete " + shorturl + "!")

                    opti = config.db.query('OPTIMIZE TABLE `keys`')
                    if not opti:
                        error.append("Couldn't optimize tables!")
                else:
                    error.append('no files given m8')
            else:
                error.append('Password for this key is wrong!!')
        else:
            error.append('Nothing to delete.')
    return template('delete', error=error, keys=x.key)


@app.route('/t/<url>')
def thumbnailer(url):
    url = url.strip()
    orig = url
    if url.split('.')[-1]:
        url = url.split('.')[0]
    if 'thumb_' + url + '.jpg' in os.listdir(config.Settings['directories']['thumbs']):
        return static_file('thumb_' + url + '.jpg', root='/var/www/bottleimg/img/t')
    else:
        results = config.db.query(
            'SELECT * FROM `keys` WHERE `url` COLLATE utf8_bin LIKE ' + config.dbn.sqlquote(url + '.%'))
        if results:
            for row in results:
                name = row['url']
                orig = row['original'] if row['original'] else row['url']
                try:
                    if 'thumb_' + url + '.jpg' in os.listdir(config.Settings['directories']['thumbs']):
                        return static_file('thumb_' + url + '.jpg', root='/var/www/bottleimg/img/t')
                    else:
                        size = 200, 200
                        base = Image.open(
                            config.Settings['directories']['private'] + name)
                        image_info = base.info
                        if base.mode not in ("L", "RGBA"):
                            base = base.convert("RGBA")
                        base = ImageOps.fit(base, size, Image.ANTIALIAS)
                        # base.thumbnail(size, Image.ANTIALIAS)
                        base.save(config.Settings['directories']['thumbs']
                                  + 'thumb_' + url + '.jpg', **image_info)
                        return static_file('thumb_' + url + '.jpg', root='/var/www/bottleimg/img/t')
                except:
                    abort(404, 'not found')
        else:
            abort(404, 'not found')
