# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
import urllib
import datetime
from tempita import looper
import hashlib
from time import mktime
import pprint
from datetime import datetime

db = config.db
Settings = config.Settings


def auth_cookies(user, hashed):
    if not bottle.request.get_cookie(user) and not bottle.request.get_cookie(hashed):
        return False
    elif bottle.request.get_cookie(hashed) == hashlib.sha1(Settings['admin'][bottle.request.get_cookie(user)]).hexdigest():
        return True
    else:
        return False


@app.get('/admin')
@app.get('/admin/')
def route():
    if auth_cookies('admin_user', 'admin_hash'):
        return template("admin")
    else:
        redirect("/admin/login")


@app.post('/admin/deletehits')
def delete_hits():
    if auth_cookies('admin_user', 'admin_hash'):
        form = AttrDict(
            key=request.forms.get('key'),
            hits=request.forms.get('hit_threshold'),
            all_keys=request.forms.get('all_keys', default="off")
        )

        if form.all_keys == "on":
            deleteQueue = config.db.query(
                'SELECT * FROM `keys` WHERE `hits` < $hits',
                vars={
                    'hits': form.hits
                }
            )
        else:
            deleteQueue = config.db.query(
                'SELECT * FROM `keys` WHERE `key` = $key AND `hits` < $hits',
                vars={
                    'key': form.key,
                    'hits': form.hits
                }
            )

        size = 0
        itemsDeleted = 0

        if deleteQueue:
            for item in deleteQueue:
                itemsDeleted = itemsDeleted + 1
                if item['is_paste'] != "y":
                    try:
                        size = size + \
                            os.stat(config.Settings['directories']
                                    ['private'] + item['url']).st_size
                        os.remove(
                            config.Settings['directories']['private'] + item['url'])
                    except:
                        print 'file', item['url'], 'does not exist.'

            if form.all_keys == "on":
                processedDelete = config.db.query(
                    'DELETE FROM `keys` WHERE `hits` < $hits',
                    vars={
                        'hits': form.hits
                    }
                )
            else:
                processedDelete = config.db.query(
                    'DELETE FROM `keys` WHERE `key` = $key AND `hits` < $hits',
                    vars={
                        'key': form.key,
                        'hits': form.hits
                    }
                )

            return str(itemsDeleted) + " items deleted. " + sizeof_fmt(size) + " of disk space saved."
        else:
            return "nothing to delete."
    else:
        redirect("/admin/login")


@app.get('/admin/login')
@app.get('/admin/login/')
def login():
    return template('admin_login')


@app.post('/admin/login')
def do_login():
    form = AttrDict(
        user=request.forms.get('key'),
        password=request.forms.get('password')
    )

    if form.user in Settings['admin'] and Settings['admin'][form.user] == form.password:
        set_cookies(
            ('admin_hash', hashlib.sha1(
                form.password).hexdigest(), (3600 * 24 * 7 * 30 * 12)),
            ('admin_user', form.user, (3600 * 24 * 7 * 30 * 12))
        )
        redirect("/admin")
    else:
        redirect("/admin/login")
