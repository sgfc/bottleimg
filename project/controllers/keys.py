# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from utils import *
import utils


@app.route('/keys')
def keys():
    _cookies = get_cookies(
        config.Settings['cookies']['key'], config.Settings['cookies']['password'])
    return template(
        'general',
        title='Keys',
        content='Key: ' + _cookies['key'] +
        '<br />Password: ' + _cookies['key_pass'],
    )
