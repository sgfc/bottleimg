# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
from mimetypes import guess_extension
from cStringIO import StringIO
import shutil
from math import ceil
import urllib
import datetime
from tempita import looper
import hashlib
from PIL import Image, ImageOps
from wsgiref.handlers import format_date_time
from time import mktime
import pprint


@app.route('/home')
def home():
    config = {
        'title': 'new tab nigga',
        'font': ('cure.se', '8pt', 'Helvetica LT Std, Helvetica Neue, Monaco, monospace'),
        'separator': '>',
        'colors': ('#020202', '#999999', '#B3B3B3', '#4C4C4C'),
        'links': {
            'boards': [
                ['facepunch', 'http://www.facepunch.com/'],
                ['/g/', 'https://boards.4chan.org/g/'],
                ['/a/', 'https://boards.4chan.org/a/'],
                ['/w/', 'https://boards.4chan.org/w/'],
            ],
            'weeb shit': [
                ['tokyotosho', 'http://tokyotosho.info/'],
                ['animebytes', 'https://animebytes.tv/'],
                ['animecalendar', 'http://animecalendar.net/']
            ],
            'sgfc': [
                ['sgfc', 'http://sgfc.co/'],
                ['stats', 'http://summerglaufc.org/wmon/']
            ],
            'misc': [
                ['git', 'https://github.com/'],
                ['maps', 'https://osu.ppy.sh/p/beatmaplist'],
                ['logs', 'https://osu.ppy.sh/p/changelog'],
                ['ck-', 'https://osu.ppy.sh/u/cookiezi']
            ],
        },
    }

    css = '''
    html, body {
        width: 99%%;
        height: 99%%;
    }

    html. body {
       margin:0;
        padding:0;
        border:0;
        height:100%%;
    }
    body {
      background-color: %s;
      font-family: "%s", %s;
      font-weight: normal;
    }
    a:link,a:visited,a:active {
      text-decoration: none;
      color: %s;
      font-weight: normal;
    }
    a:hover {
      text-decoration: underline;
      color: %s;
      font-weight: normal;
    }
    table {
      font-size: %s;
      border-spacing: 8px;
      margin-left: 7%%;
    }
    td {
      vertical-align: middle;
    }
    td:first-child {
      font-weight: normal;
      color: %s
    }
    td:nth-child(2) {
      font-weight: normal;
      color: %s;
    }
    td span:after {
      content: " - "
    }
    td span:last-child:after {
      content: "";
    }
    ''' % (config['colors'][0], config['font'][0], config['font'][2], config['colors'][1], config['colors'][1],
           config['font'][1], config['colors'][2], config['colors'][3])

    links_html = ''
    for group in sorted(config['links']):
        links_html += '<tr><td align="right">%s</td><td>%s</td><td>' % (group,
                                                                        config['separator'])
        for site in config['links'][group]:
            links_html += '<span class="wrap"><a href="%s">%s</a></span> ' % (site[1],
                                                                              site[0])
        links_html += '</td></tr>'

    html = '''<!DOCTYPE html>
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>%s</title>
        <style>%s</style>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>
            function fuck(vars){
                $('.music').html(vars.now_playing.meta + ' ('+ vars.now_playing.playcount +' plays)')
            }
            $(document).ready(function() {
                var refreshId = setInterval(function() {
                    $.getJSON( "http://127.0.0.1:8888/vakata2/?param3=smallresponse.json&callback=?", function( json ) {});
                }, 200);
                $.ajaxSetup({ cache: false });
            });
        </script>
      </head>
      <body>
        <table summary="container" border="0" width="94%%" height="100%%"><tr><td><table summary="container">
          %s
          <tr><td align="right">now playing</td><td>></td><td><span class="music">placeholder</span></td></tr>
          <tr><td align="right">search</td><td>></td><td><form action="https://www.google.com/search" name="searchbox" method="get" /><input maxlength="256" size="60" tabindex="1" name="q" value="" accesskey="a" placeholder="Search Google..." id="qqqqq" style="background: none;color: rgb(128,128,128);font-family: inherit;font-size: inherit;border:none;" /></form>
          </td></tr>
        </table></td></tr></table>
      </body>
    </html>''' % (config['title'], css, links_html)

    # return render.home(links)
    request.content_type = 'text/html; charset=utf-8'
    return html
