# -*- coding: utf-8 -*-
from project import app, config
from bottle import request, response, get, post, redirect, abort, static_file
from bottle import mako_view as view, mako_template as template
from random import choice
import string
import os
import sys
import re
from utils import *
import utils
import magic
import json
from mimetypes import guess_extension
from cStringIO import StringIO
import shutil
from math import ceil
import urllib
import datetime
from tempita import looper
import hashlib
from PIL import Image, ImageOps
from wsgiref.handlers import format_date_time
from time import mktime
import pprint


@app.get('/settings')
def show_settings():
    _cookies = get_cookies(
        config.Settings['cookies']['key'], config.Settings['cookies']['password'])
    current_settings = [0, 'date_desc', 0, '']
    settings = config.db.query(
        """
        SELECT `settings`.*, `keys`.passs
        FROM `keys`
        INNER JOIN `settings`
        ON `settings`.`key`=`keys`.`key` WHERE `settings`.`key` = $key  and `keys`.passs = $password
        """, vars={'key': _cookies['key'], 'password': _cookies['key_pass']})
    if settings:
        set = settings[0]
        current_settings = [
            set['block_access'],
            set['sort_mode'],
            set['view_mode'],
            set['password']
        ]
        print current_settings
    return template('settings', cookie_key=_cookies['key'], cookie_pass=_cookies['key_pass'], sortmodes=utils.sortmodes, current=current_settings)


@app.post('/settings')
def set_settings():
    i = AttrDict(
        confirm_key=request.forms.get('confirm_key'),
        confirm_pass=request.forms.get('confirm_pass'),
        gallery_password=request.forms.get('gallery_password'),
        password=request.forms.get('password'),
        sort=request.forms.get('sort'),
        block=request.forms.get('block'),
        view=request.forms.get('view'),
    )
    if not i.confirm_key or not i.confirm_pass:
        return template('general', title='Error!', content="You didn't enter your key/password for confirmation!")
    elif i.confirm_key == 'public':
        return template('general', title='Error!', content="i dont think so m8")
    else:
        change_pass_q = config.db.query(
            'SELECT * FROM `keys` WHERE `key` = $key AND `passs` = $passs',
            vars={'key': i.confirm_key, 'passs': i.confirm_pass})
        if not i.password:
            passssss = i.confirm_pass

        if not i.gallery_password:
            passssss = i.confirm_pass
        else:
            passssss = i.gallery_password
        if change_pass_q:
            sql_pass = change_pass_q[0]['passs']
            if sql_pass == i.confirm_pass:
                if sql_pass == i.password:
                    return template('general', title='Error!', content="Now what's the point in setting the password to the same as it is currently?")
                else:
                    settings_q = config.db.query(
                        'SELECT * FROM `settings` WHERE `key` = $key', vars={'key': i.confirm_key})
                    if i.sort not in utils.sortmode:
                        sort = 'date_desc'
                    else:
                        sort = i.sort

                    password = passssss
                    key_password = i.password or i.confirm_pass
                    if not settings_q:
                        results = config.db.query(
                            'INSERT INTO `settings` (`key`, `password`, `sort_mode`, `block_access`, `view_mode`) VALUES ($key,$password,$sort_mode,$block,$view)',
                            vars={'key': i.confirm_key, 'password': password, 'sort_mode': sort, 'block': i.block, 'view': i.view})
                    else:
                        results = config.db.query(
                            'UPDATE `settings` SET `password` = $passs, `sort_mode` = $sort_mode, `block_access` = $block, `view_mode` = $view WHERE `key` = $key',
                            vars={'key': i.confirm_key, 'passs': password, 'sort_mode': sort, 'block': i.block, 'view': i.view})
                        if i.password:
                            change_global_pass = config.db.query(
                                'UPDATE `keys` SET `passs` = $passs WHERE `key` = $key',
                                vars={'key': i.confirm_key, 'passs': key_password})
                    if results or change_global_pass:
                        set_cookies(
                            (config.Settings['cookies']['key'], i.confirm_key),
                            (config.Settings['cookies']['password'], key_password))
                        return template('general', title='Success!', content="Your settings for the key \"" + i.confirm_key + "\" has been set!")
                    else:
                        return template('general', title='Error!', content="something went wrong lel")
            else:
                return template('general', title='Error!', content="You supplied an incorrect confirmation password!")
        else:
            return template('general', title='Error!', content="That key doesn't exist!")
