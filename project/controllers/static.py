# -*- coding: utf-8 -*-
from project import app, config
from bottle import static_file, request
from bottle import mako_view as view, mako_template as template, MakoTemplate
from utils import *


@app.route('/', method='GET')
def index():
    cookies = get_cookies(config.Settings['cookies']['key'],
                          config.Settings['cookies']['password'])
    # MakoTemplate.default_filters = ['trim']
    return template('index', key=cookies['key'], key_pass=cookies['key_pass'])


@app.route('/:file#(favicon.ico)#')
def favicon(file):
    return static_file(file, root='static/misc')


@app.route('/static/:path#(images|css|js|fonts)\/.+#')
def server_static(path):
    return static_file(path, root='static')
