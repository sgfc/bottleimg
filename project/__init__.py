# -*- coding: utf-8 -*-
__version__ = '0.1'
from bottle import *
import config
import os
import glob
import magic
app = Bottle()
TEMPLATE_PATH.append(config.Settings['directories']['based']+'/project/views/')
TEMPLATE_PATH.remove('./views/')
print TEMPLATE_PATH
from project.controllers import *

def _file_iter_range(fp, offset, bytes, maxread=1024*1024):
    ''' Yield chunks from a range in a file. No chunk is bigger than maxread.'''
    fp.seek(offset)
    while bytes > 0:
        part = fp.read(min(bytes, maxread))
        if not part: break
        bytes -= len(part)
        yield part

def local_static_file(filename, root, mimetype='auto', download=False):
    """ Open a file in a safe way and return :exc:`HTTPResponse` with status
        code 200, 305, 401 or 404. Set Content-Type, Content-Encoding,
        Content-Length and Last-Modified header. Obey If-Modified-Since header
        and HEAD requests.
    """
    root = os.path.abspath(root) + os.sep
    filename = os.path.abspath(os.path.join(root, filename.strip('/\\')))
    headers = dict()

    if not filename.startswith(root):
        return abort(403, "Access denied.")
    if not os.path.exists(filename) or not os.path.isfile(filename):
        return abort(404, "File does not exist.")
    if not os.access(filename, os.R_OK):
        return abort(403, "You do not have permission to access this file.")

    if mimetype == 'auto':
        mimetype = magic.from_file(filename, mime=True)
        if mimetype: headers['Content-Type'] = mimetype
    elif mimetype:
        headers['Content-Type'] = mimetype

    headers['Content-Disposition'] = 'inline; filename="%s"' % download

    stats = os.stat(filename)
    headers['Content-Length'] = clen = stats.st_size
    lm = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime(stats.st_mtime))
    headers['Last-Modified'] = lm

    ims = request.environ.get('HTTP_IF_MODIFIED_SINCE')
    if ims:
        ims = parse_date(ims.split(";")[0].strip())
    if ims is not None and ims >= int(stats.st_mtime):
        headers['Date'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
        return HTTPResponse(status=304, **headers)

    body = '' if request.method == 'HEAD' else open(filename, 'rb')

    headers["Accept-Ranges"] = "bytes"
    ranges = request.environ.get('HTTP_RANGE')
    if 'HTTP_RANGE' in request.environ:
        ranges = list(parse_range_header(request.environ['HTTP_RANGE'], clen))
        if not ranges:
            return HTTPError(416, "Requested Range Not Satisfiable")
        offset, end = ranges[0]
        headers["Content-Range"] = "bytes %d-%d/%d" % (offset, end-1, clen)
        headers["Content-Length"] = str(end-offset)
        if body: body = _file_iter_range(body, offset, end-offset)
        return HTTPResponse(body, status=206, **headers)
    return HTTPResponse(body, **headers)

@app.route('/<url>')
def image_view(url):
    glob = []
    try:
        os.chdir(config.Settings['directories']['private'])
        if '.' in url:
            glob = glob.glob(os.path.join("%s*" % url.split('.')[0]))
            url = glob[0]
    except:
        if '.' in url:
            url = url.split('.')[0]
        else:
            url = url
    os.chdir(config.Settings['directories']['based'])
    results = config.db.query('SELECT * FROM `keys` WHERE `url` COLLATE utf8_bin LIKE ' + config.dbn.sqlquote(url + '.%') )
    if results or glob != [] or glob:
        res = results[0]
        url = res['url']
        original = res['original']
        config.db.query('UPDATE `keys` SET hits = hits + 1 WHERE `url` = ' + config.dbn.sqlquote(url) )
    else:
        url = ''
        original = ''
    print original
    return local_static_file(url, root='/var/www/bottleimg/img/p', download=original)
