from web import database
from web import db as dbn

base = "/var/www/bottleimg" #base folder

Settings = {
    "directories": {
        "private": base + "/img/p/", #private image location
        "thumbs": base + "/img/t/", #thumbnail location
        "url": "http://example.com", #homeurl
        "based": base
    },
    "database": {
        "user": "username", #database user
        "password": "password", #db user password
        "db": "example" #database name
    },
    "cookies": {
        "key": "key", #key cookie name
        "password": "key_pass", #password cookie name
        "expire": 3600*24*7*30*12 #expiration time
    },
    "admin": {
        "USERNAME": "PASSWORD", #username and password for admin panel
        "ANOTHER_USER": "ANOTHER_PASSWORD" #another user
    }
}

# Database object
db = database(
    dbn='mysql',
    db=Settings['database']['db'],
    user=Settings['database']['user'],
    pw=Settings['database']['password'],
    charset='utf8'
)
