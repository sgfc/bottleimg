<!DOCTYPE html>
<html>
    <head>
        <%include file="head.tpl" args="title=title"/>
    % if css:
        <style type="text/css">
            ${css}
        </style>
    % endif
    </head>
    <body class="general">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                    <header>
                        ${title}
                        <div><a href="/">Home</a> / <a href="/paste">Pastebin</a> / <a href="/gallery">Public Gallery</a> / <a href="/changelog">Changelog</a> / <a href="http://summerglaufc.org/wmon/">Server Stats</a></div>
                    </header>
                    <div id="main">
                        ${content}
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>