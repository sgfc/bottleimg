<%!
    def neighborhood(iterable):
        iterator = iter(iterable)
        prev = None
        item = iterator.next()  # throws StopIteration if empty.
        for next in iterator:
            yield (prev,item,next)
            prev = item
            item = next
        yield (prev,item,None)
%>
<!DOCTYPE html>
<html>
    <head>
        <%include file="head.tpl" args="title=title"/>
        % if css:
        <style type="text/css">
            ${css}
        </style>
        % endif
    </head>

    <body class="paste">
        <div id="container">
            <header>
                ${title}
            </header>
            <div id="main">
                <%
                    count = content[0]
                    lines = content[1]
                    hits = content[3] + 1
                %>
                <div class="boardlist">[ Characters: ${count} / Lines: ${lines} / Hits: ${hits} / Language: ${lang} ]</div>
                % if not edit:
                    <div class="boardlist" style="float:right;">[
                        % if sect:
                            Sections:
                            % for prev,item,next in neighborhood(sect):
                                <a href="#${item[0]}">${item[1]}</a> |
                            % endfor
                        % endif
                        % if url:
                            <a href="/paste/${url}/raw">Raw paste</a>
                        % endif
                     ]</div>
                    <div class="allcode">
                        ${content[2]}
                    </div>
                % else:
                    <div class="allcode">
                        <textarea tabindex="20" rows="22" name="paste_edit_body" id="paste_edit_body" cols="40" class="pastebox">${content[2]}</textarea>
                    </div>
                % endif
            </div>
        </div>
    </body>
</html>