<!DOCTYPE html>
<html>
    <head>
        <%include file="head.tpl" args="title=Settings"/>
        <style>
            #main a {
                text-decoration: underline !important;
            }
        </style>
    </head>

    <body class="settings">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                    <header>
                        Settings
                        <div><a href="/">Home</a> / <a href="/paste">Pastebin</a> / <a href="/gallery">Public Gallery</a> / <a href="/changelog">Changelog</a> / <a href="http://summerglaufc.org/wmon/">Server Stats</a></div>
                    </header>
                    <div id="main">
                            <form action="" method="post">
                                    Enter your present details in order to make changes <p style="float:right;margin:3px 0;">You can find your current details at <a href="/keys">/keys</a></p><br/><br/>
                                    <label>Present key:</label><input type="text" class="textbox rightcol" value="${cookie_key}" name="confirm_key"><br />
                                    <label>Present password:</label><input type="text" class="textbox rightcol" value="${cookie_pass}" name="confirm_pass">
                                    <br /><br />
                                <h2>Change key details</h2>
                                    <label for="password">New Password:</label><span class="rightcol"><input type="text" class="textbox" value="" name="password"></span>
                                <br />
                                <h2>Gallery settings</h2>
                                    <label>Default sort mode:</label>
                                        <select name="sort" id="sort" class="rightcol">
                                            % for e in sortmodes:
                                                % if e[0] == current[1]:
                                                    <option value="${e[0]}" selected="selected">${e[1]}</option>
                                                % elif current == [] and e[0] == 'date_desc':
                                                    <option value="${e[0]}" selected="selected">${e[1]}</option>
                                                % else:
                                                    <option value="${e[0]}">${e[1]}</option>
                                                % endif
                                            % endfor
                                        </select>
                                    <br />
                                    <label>Default view mode:</label>
                                        <ul class="rightcol">
                                            <li><label><input type="radio"
                                                % if not current[2]:
                                                    checked="checked" \
                                                % endif
                                            value="0" name="view"> <a href="http://sgfc.co/gallery?view=0">Default</a></label></li>
                                            <li><label><input type="radio"
                                                % if current[2]:
                                                    checked="checked" \
                                                % endif
                                            value="1" name="view"> <a href="http://sgfc.co/gallery?view=1">Content List</a></li>
                                        </ul><br />
                                    <label>Block unauthorized entry:</label>
                                        <ul class="rightcol">
                                            <li><label><input type="radio"
                                                % if not current[0]:
                                                    checked="checked" \
                                                % endif
                                            value="0" name="block"> Allow anyone to view your gallery</label></li>
                                            <li><label><input type="radio"
                                                % if current[0]:
                                                    checked="checked" \
                                                % endif
                                            value="1" name="block"> Require password to view your gallery</label></li>
                                        </ul><br />
                                    <label>Gallery password:</label>
                                        <span class="rightcol">
                                            <input type="text" class="textbox" value="${current[3]}" name="gallery_password">
                                            <p>This is only needed if "Require password to view your gallery" is set. <br/>This password is for logging into the gallery, and does not change your key password.</p>
                                        </span>
                                    <br /><input type="submit" value="Save Changes" class="button" style="float:right;"><br />
                            </form>
                    </div>
                </td>
            </tr>
        </table>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
        <!--<script src="http://summerglaufc.org/js/script.js"></script>-->
    </body>
</html>