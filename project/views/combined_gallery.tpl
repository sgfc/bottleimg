## -*- coding: utf-8 -*-
## -*- encoding:utf-8 -*-
<%!
    import markupsafe
    import re

    def hl(text, search):
        output = ''
        i = 0
        text = markupsafe.escape(text)
        regex = re.compile(r"("+ re.escape(search) +")", re.I)
        for m in regex.finditer(text):
            output += "".join([text[i:m.start()],
                               "<span style='background-color:%s'>" % 'yellow',
                               text[m.start():m.end()],
                               "</span>"])
            i = m.end()
        return "".join([output, text[i:]])

    def sizeof_fmt(num, short=None):
        for x in ['bytes','KB','MB','GB']:
            if num < 1024.0 and num > -1024.0:
                return "%3.2f %s" % (num, x) if not short else "%3.0f %s" % (num, x)
            num /= 1024.0
        return "%3.1f %s" % (num, 'TB')

    def ireplace(old, new, text):
        idx = 0
        while idx < len(text):
            index_l = text.lower().find(old.lower(), idx)
            if index_l == -1:
                return text
            text = text[:index_l] + new + text[index_l + len(old):]
            idx = index_l + len(old)
        return text
%>
<%def name="do_pages(pageq, keyzq, pagesq, lelq, lelnextq, queryq, view_idq)">
    % if pagesq != 1:
        % if pageq == 1 or lelq == False:
            <span class="disabled"></span>
        % else:
        <%
            print pageq, keyzq, pagesq, lelq, lelnextq, queryq, view_idq
        %>
            <a href="/gallery/${keyzq}${queryq}${lelq}&view=${view_idq}" data-page="${lelq}"> << </a>
        % endif
        % for page_numberq in range(1, pagesq + 1):
            <a href="/gallery/${keyzq}${queryq}${page_numberq}&view=${view_idq}" data-page="${page_numberq}">
                % if pageq == page_numberq:
                    <span style="color: #090910;font-weight: bold !important;"> ${page_numberq} </span>
                % else:
                    ${page_numberq}
                % endif
            </a>
        % endfor
        % if lelnextq == pagesq + 1 or lelnextq == False:
            <span class="disabled"></span>
        % else:
            <a href="/gallery/${keyzq}${queryq}${lelnextq}&view=${view_idq}" data-page="${lelnextq}"> >> </a>
        % endif
    % endif
</%def>
<%
    orig_keyz = thisk
    keyz = thisk

    if keyz == 'public':
        keyz = ''

    view_id = 0 if type == 'testgallery' else 1

    query = re.sub('\?page=(\d+|)', '', querys)
    query = re.sub('\?view=(\d+|)', '', querys)
    if query == "" or query == None or query == False:
        query = '?page='
    else:
        query = re.sub('&page=(\d+|)', '', query)
        query = re.sub('&view=(\d+|)', '', query)
        query = query + '&page='

    if query[0] == '&':
        q_mod = list(query)
        q_mod[0] = '?'
        query = "".join(q_mod)

    searchmodes = (
        ('filename', 'Filename'),
        ('url', 'Short URL'),
        ('content', 'Paste Content')
    )
%>
% if not pjax:
    <!DOCTYPE html>
    <html>
        <head>
            <link href="data:image/x-icon;base64,AAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAA//36AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAREREREQAAABERERERAAAAAAAAAAAAAAAAABEAAAAAAAABERAAAAAAABEREQAAAAABEREREAAAABERERERAAAAAAEREAAAAAAAAREQAAAAAAABERAAAAAAAAEREAAAAAAAAREQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" rel="icon" type="image/x-icon" />
            <link href='/static/css/style.css' rel='stylesheet' type='text/css'>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            % if orig_keyz[-1] == 's':
                <title>SGFC >> ${orig_keyz}' Gallery</title>
            % elif orig_keyz == 'public':
                <title>SGFC >> Public Gallery</title>
            % else:
                <title>SGFC >> ${orig_keyz}'s Gallery</title>
            % endif
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
            <script src="//browserstate.github.io/history.js/scripts/bundled/html4+html5/jquery.history.js"></script>
            <style type="text/css">
                #error { padding: 20px; }
                .highlight { background: yellow; }
            </style>
            % if type == 'testgallery':
            <style>
                .info
                {
                    background: none repeat scroll 0 0 #FFFFFF;
                    box-shadow: 0 0 1px rgba(0, 0, 0, 0.8) inset, 0 2px 0 rgba(255, 255, 255, 0.5) inset, 0 -1px 0 rgba(0, 0, 0, 0.4) inset;
                    color: #000000;
                    display: inline-block;
                    font-size: 75%;
                    margin-top: 2px;
                    overflow: hidden;
                    padding: 2px 4px;
                    $if public:
                        text-align: center;
                    $else:
                        text-align: right;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    width: 192px;
                    position: relative;
                    top: 201px;
                }

                body.gallery .wrapper {
                    margin-bottom: 50px;
                }

                body.gallery h2#debug.top { margin-bottom: 20px }
            </style>
            % else:
            <style>
                body.gallery.gc .info {
                    -moz-box-sizing: border-box;
                    box-shadow: none;
                    color: rgb(0, 0, 0);
                    display: block;
                    float: left;
                    font-size: 150%;
                    line-height: 80px;
                    margin-left: 0px;
                    overflow: hidden;
                    padding: 0px 11px;
                    text-align: left !important;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    max-width: 68%;
                    width: auto;
                }

                .img > a {
                    width: 80px;
                    height: 80px;
                    position: absolute;
                }

                body.gallery.gc .info.details {
                    color: rgba(0, 0, 0, 0.5);
                    display: block;
                    font-size: 14px;
                    line-height: 19px;
                    padding: 0px;
                    margin-top: 13px;
                    float: right;
                    text-align: center;
                    width: auto;
                    max-width: 100%;
                }

                @media all and (max-width: 425px){
                    body.gallery.gc .img {
                        float:none;
                        display:block;
                        margin:0 auto;
                        width: 200px;
                        height: 200px;
                    }

                    body.gallery.gc .paste {
                        width: 200px;
                        height: 199px;
                    }

                    .img > a {
                        width: 200px !important;
                        height: 200px !important;
                    }

                    body.gallery.gc .checkbawks {
                        display:block;
                        margin: 10px auto 0;
                    }

                    body.gallery.gc .info {
                        float:none;
                        display:block;
                        clear:both;
                        max-width: 90%;
                        margin: 0 auto;
                        padding: 0 1px;
                        text-align:center !important;
                        width: 100%;
                        white-space: nowrap;
                        text-overflow: ellipis;
                        line-height: 2em;
                    }

                    body.gallery.gc .info a {
                        display:inline;
                        width: 100%;
                        text-align: center;
                        line-height: 40px;
                        margin-top: 2px;
                    }

                    body.gallery.gc .info.details{
                        width:100%!important;
                        float:none!important;
                        padding:10px 0 0!important;
                        margin-top: 0px;
                    }
                }
            </style>
            % endif
        </head>

        <body class="gallery\
        % if type == 'gallery_content':
            gc\
        % endif
        ">
            <div id="container">
                <header>
                    Gallery
                </header>
                <div id="main">
% endif
% if pages != 1:
    <h2 id="pages" style="position:relative;">
        ${do_pages(page, keyz, pages, lel, lelnext, query, view_id)}
    </h2>
% endif
<script>
    var current_page = ${page};
</script>
<h2 id="debug" class="top">
    <form class="sorty" action="" method="get" style="display: block;margin-top: 3px;text-align: center;">
        <select name="sort" id="sort">
            % for e in sortmodes:
                % if e[0] == sortmode:
                    <!-- ${e[0]} | ${sortmode} -->
                    <option value="${e[0]}" selected>${e[1]}</option>
                % else:
                    <!-- ${e[0]} | ${sortmode} -->
                    <option value="${e[0]}">${e[1]}</option>
                % endif
            % endfor
        </select>
        <!--<input type="hidden" name="page" value="${page}" />-->
        <input type="hidden" name="view" value="${view_id}" />
        <input type="submit" name="do_sort" value="Sort" />

        <div style="margin:10px auto;position:relative">
            <span class="case" style="display: inline-block; position: absolute; top: 22px; left: auto; margin-left: -4px;">
                <input style="vertical-align: bottom; margin-top: 3px;" type="checkbox" name="case" value="1"
                % if case:
                    checked
                % endif
                >Case-sensitive search
            </span>
            <input type="text" name="query" placeholder="search query"
            % if queryz:
                value="${queryz}"
            % endif
            />
                in
            <select name="query_in">
                % for e in searchmodes:
                    % if e[0] == qryi:
                        <!-- ${e[0]} | ${sortmode} -->
                        <option value="${e[0]}" selected>${e[1]}</option>
                    % else:
                        <!-- ${e[0]} | ${sortmode} -->
                        <option value="${e[0]}">${e[1]}</option>
                    % endif
                % endfor
            </select>
            <input type="submit" name="do_search" value="Search" />
        </div>
        <div style="margin-top:20px">View:
            % if type != "testgallery":
                <a href="${query}${page}&view=0">Default</a> / <span>Content List</span>
            % else:
                <span>Default</span> / <a href="${query}${page}&view=1">Content List</a>
            % endif
        </div>
    </form>

</h2>
<div class='loader' style="display:none;">
    <img src="data:image/gif;base64,R0lGODlhIAAgAPMAAP///wAAAMbGxoSEhLa2tpqamjY2NlZWVtjY2OTk5Ly8vB4eHgQEBAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQJCgAAACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQJCgAAACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkECQoAAAAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkECQoAAAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAkKAAAALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQJCgAAACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAkKAAAALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQJCgAAACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkECQoAAAAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOwAAAAAAAAAAAA==" style="margin: 50px;" />
</div>
<form action="/gallery/delete" method="post" class="main_form">
    <input type="hidden" name="key" value="${keyz}" />
    % if error:
        <div id='error'>${error}</div>
    % else:
        % for url in files:
            <%
                nurl = url[0].split(';')

                durl = nurl[0]
                thepaste = nurl[1]

                original = url[1]
                old_orig = original
                shorturl = url[2]
                dimensions = url[3]
                size = url[4]
                mod_time = url[5]
                item_type = url[6]
                hits = url[7]

                if queryz:
                    original = hl(original, queryz)
                    durl = hl(durl, queryz)

                # original = re.sub('(?i)' + re.escape(queryz), '<span class="highlight">'+queryz+'</span>', original)
                # durl = re.sub('(?i)' + re.escape(queryz), '<span class="highlight">'+queryz+'</span>', durl)
                # original = original.replace(queryz,'<span class="highlight">'+queryz+'</span>')
                # durl = durl.replace(queryz,'<span class="highlight">'+queryz+'</span>')
                url = shorturl

                if thepaste == "1" or thepaste == 'y':
                    paste = True
                    strt = '/paste/'
                    if type == "gallery_content":
                        original = original[:75] + (original[75:] and '...')
                    else:
                        original = original[:200] + (original[200:] and '...')
                    item_type = 'Paste'
                else:
                    paste = False
                    #strt = '/p/'
                    strt = '/'

                if (nurl[2] == "1" or nurl[2] == 'y') and (nurl[1] == "0" or nurl[1] == "n"):
                    file = True
                    strt = '/'
                else:
                    file = False

                image = True if nurl[2] == "0" else False
            %>
            % if type == "gallery_content":
                <div class="wrapper clearfix">
                    % if paste or file:
                        <div class="img">
                    % else:
                        <div class="img" style="background-image: url('/t/${shorturl}')">
                    % endif
                    % if file:
                        <a title="${old_orig|h}" href="$strt$shorturl" style="line-height: 80px; font-size: 2em; font-weight: bold;">
                            FILE
                    % elif paste:
                        <a title="${durl|h}" href="${strt}${shorturl}">
                    % else:
                        <a title="${old_orig|h}" href="${strt}${shorturl}">
                    % endif
                    % if paste:
                        <span class="paste">
                            ${original|h}
                        </span>
                        <!--<img width="100%" height="100%" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="opacity: 0;position: relative;top: -203px;">-->
                    % elif file:
                        <!-- lel -->
                    % else:
                        <!--<img width="100%" height="100%" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="opacity: 0">-->
                    % endif
                    </a>
                </div>
                <span class="info">
                    % if not public:
                        <input type="checkbox" name="delete_this" value="${shorturl}" class="checkbawks">
                    % endif
                    <!-- ${file} | ${nurl[2]} -->
                    % if paste:
                        Paste: <a title="${shorturl}" href="${strt}${shorturl}">${durl}</a>
                    % elif file:
                        File: <a title="${shorturl}" href="${strt}${shorturl}">${original}</a>
                    % else:
                        <a title="${shorturl}" href="${strt}${shorturl}">${original}</a>
                    % endif
                </span>
                <span class="info details">
                    Size:
                    % if size != []:
                        % if paste:
                            <span>${size} characters</span>
                        % else:
                            <span>${sizeof_fmt(size)}
                            % if dimensions == [] or dimensions == '[]':
                                <!-- no -->
                            % else:
                                (${dimensions[0]} x ${dimensions[1]})
                            % endif
                            </span>
                        % endif
                    % else:
                        could not be found.
                    % endif
                    <br />
                    Uploaded:
                    % if paste:
                        <span>>upload time on pastes</span>
                    % else:
                        % if mod_time:
                            <span>${mod_time}</span>
                        % else:
                            time could not be found.
                        % endif
                    % endif
                    <br />
                    Hits: <span>${hits}</span>
                </span>
                </div>
            % else:
                <div class="wrapper">
                    % if paste or file:
                        <div class="img">
                    % else:
                        <div class="img" style="background-image: url('/t/${shorturl}')">
                    % endif
                    % if file:
                        <a title="${old_orig|h}" href="${strt}${shorturl}" style="height: 200px; position: absolute; width: 200px;font-size:92px;line-height:200px">
                            FILE
                    % elif paste:
                        <a title="${durl|h}" href="${strt}${shorturl}" style="height: 200px; position: absolute; width: 200px;">
                    % else:
                        <a target="_blank" title="${old_orig|h}" href="${strt}${shorturl}" style="height: 200px; position: absolute; width: 200px;">
                    % endif
                    % if paste:
                        <span class="paste">
                            ${original|h}
                        </span>
                        <img width="100%" height="100%" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="opacity: 0;position: relative;top: -203px;">
                    % elif file:
                        <!-- lel -->
                    % else:
                        <img width="100%" height="100%" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" style="opacity: 0">
                    % endif
                    </a>
                    <span class="info">
                        % if not public:
                            <input type="checkbox" name="delete_this" value="${shorturl}" class="checkbawks">
                        <!-- $file | $nurl[2] -->
                        % endif
                        % if paste:
                            Paste: <a title="${shorturl}" href="${strt}${shorturl}">${durl}</a>
                            % if shorturl != durl:
                                (${shorturl})
                            % endif
                        % elif file:
                            File: <a title="${shorturl}" href="${strt}${shorturl}">${original}</a> (${shorturl})
                        % else:
                            <a title="${shorturl}" href="${strt}${shorturl}">${original}</a>
                        % endif
                        <br />
                        <span style="color:rgba(0, 0, 0, 0.5);">Hits:</span> ${hits}
                    </span>
                </div>
                </div>
            % endif
        % endfor
    % endif
    % if pages != 1:
        <!-- $lel | $lelnext -->
        <h2 id="pages" class="bottom">
            <!-- $limit,30 -->
            ${do_pages(page, keyz, pages, lel, lelnext, query, view_id)}
        </h2>
    % endif
    % if not public:
        <h2 id="debug">
            <div>
                delete selected images: <input type="password" value="" name="password" placeholder="keyz password" /> <input type="submit" name="delete" value="Delete" />
            </div>
        </h2>
    % endif
</form>
% if not pjax:
            </div>
        </div>
        <script>
             // $$(this).attr('href')
             var current_page;
             History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state
                History.log('statechange:', State.data, State.title, State.url);
                $('.main_form').fadeOut(100);
                $('.loader').fadeIn(100);
                $.ajax({
                    url: State.url,
                    beforeSend: function(jqXHR, settings) {
                        jqXHR.setRequestHeader('X-AJAX', 'true');
                    },
                    // You need to manually do the equivalent of "load" here
                    success: function(result) {
                        $('.loader').hide();
                        $("#main").html(result);
                    }
                });
                current_page = State.data.state;
                console.log(current_page);
            });

            $(document).ready(function() {
                $(document).on('click','#pages a',function(e){
                    e.preventDefault();
                    console.log('prevented click' + e);
                    console.log($(this).attr('data-page') + ' != ' + current_page);

                    a_this = $(this)

                    if($(this).attr('data-page') != current_page) {
                        History.pushState({state:$(this).attr('data-page')}, $(document).attr('title'), $(this).attr('href')); // logs {state:1}, "State 1", "?state=1"
                    }
                })
            });

            // $$(document).pjax('#pages a', '#main')
        </script>
    </body>
% endif
</html>
