<%page args="title=''"/>
<title>\
%if title:
SGFC >> ${title}\
%else:
SGFC File Upload\
% endif
</title>
        <link rel="icon" type="image/ico" href="/favicon.ico" />
        <link href='/static/css/style.css' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1">