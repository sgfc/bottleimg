<!doctype html>
    <head>
        <%include file="head.tpl" args="title='Error!'"/>
    </head>
    <body class="general">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                    <header>
                        Error!
                    </header>
                    <div id="main">
                        % if error:
                            ${error|h,trim}
                        % endif
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>