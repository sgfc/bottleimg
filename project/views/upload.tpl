<!doctype html>
    <head>
        <%include file="head.tpl" args="title='Upload'"/>
        <style>
            #main {
                text-align: center;
            }
        </style>
    </head>

    <body class="general">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                    <header>
                        Upload status
                    </header>
                    <div id="main">
                    % if key == 'public':
                        URL: <a href="${baseurl}/${url}">${baseurl}/${url}</a><br />
                        Gallery: <a href="/gallery/${key}">${baseurl}/gallery</a>
                    % else:
                        URL: <a href="${baseurl}/${url}">${baseurl}/${url}</a><br />
                        Gallery: <a href="/gallery/${key}">${baseurl.replace('/paste','')}/gallery/${key}</a>
                    % endif
                    </div>
                </td>
            </tr>
        </table>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
        <!--<script src="http://summerglaufc.org/js/script.js"></script>-->
    </body>
</html>