<!DOCTYPE html>
<html>

    <head>
        <title>Delete</title>
        <link href="data:image/x-icon;base64,AAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAA//36AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAREREREQAAABERERERAAAAAAAAAAAAAAAAABEAAAAAAAABERAAAAAAABEREQAAAAABEREREAAAABERERERAAAAAAEREAAAAAAAAREQAAAAAAABERAAAAAAAAEREAAAAAAAAREQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" rel="icon" type="image/x-icon" />
        <link href='/static/css/style.css' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body class="general">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                    <header>
                        Delete status
                    </header>
                    <div id="main">
                        <ul>
                        % for i in error:
                            <li>${i}</li>
                        % endfor
                        </ul><br />
                        <a href="/gallery/${keys}">Return to the gallery</a>
                    </div>
                </td>
            </tr>
        </table>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
        <!--<script src="http://summerglaufc.org/js/script.js"></script>-->
    </body>
</html>