<!DOCTYPE html>
<html>
    <head>
        <%include file="head.tpl" args="title=title"/>
    </head>
    <body class="pastebin">
        <table id="maintb" cellpadding="0" cellspacing="0">
            <tr>
                <td id="maintd">
                  <header>
                      ${title}
                      <div><a href="/">Home</a> / <a href="/gallery">Public Gallery</a> / <a href="/gallery/$key">Your Gallery</a> / <a href="/keys">Your Keys</a> <br /><a href="/changelog">Changelog</a> / <a href="http://summerglaufc.org/wmon/">Server Stats</a></div>
                  </header>
                  <div id="main">
                      <form action="/upload/paste" method="post">
                          <label>Name:</label> <input type="text" name="paste_name" id="paste_name" placeholder="(optional)" />
                          <label for="lang">Language:</label>
                          <select name="lang" id="lang">
                                <option value="bbcode">BBCode</option>
                                <option value="bash">Bash</option>
                                <option value="bat">Batchfile</option>
                                <option value="brainfuck">Brainfuck</option>
                                <option value="c">C</option>
                                <option value="csharp">C#</option>
                                <option value="cpp">C++</option>
                                <option value="css">CSS</option>
                                <option value="diff">Diff</option>
                                <option value="html">HTML</option>
                                <option value="html+php">HTML+PHP</option>
                                <option value="ini">INI</option>
                                <option value="irc">IRC logs</option>
                                <option value="java">Java</option>
                                <option value="js">JavaScript</option>
                                <option value="lua">Lua</option>
                                <option value="multi">Multiple</option>
                                <option value="mysql">MySQL</option>
                                <option value="nginx">Nginx configuration file</option>
                                <option value="php">PHP</option>
                                <option value="python">Python</option>
                                <option value="text" selected>Plain text</option>
                          </select>
                          <br /><br />

                          <div id="filess">
                              <textarea tabindex="20" rows="22" name="paste_body" id="paste_body" cols="40" class="pastebox"></textarea>
                          </div>
                          <br />
                          <input type="submit" name="submit" value="Paste" />
                          <br /><br />
                          <input type="radio" name="type" value="public" id="pub" checked>&nbsp;Public
                          <input type="radio" name="type" value="private" id="priv">&nbsp;Private

                          <div id="identification">
                              <label>Key</label>&nbsp;<input type="text" size="20" value="${keyc}" name="key" id="key" />
                              <br />
                              <label>Password</label>&nbsp;<input type="password" size="20" value="${passc}" name="password" id="password" />
                          </div>
                          <input type="hidden" name="sharex" id="sharex" value="no" />
                      </form>
                  </div>
                </td>
            </tr>
        </table>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
        <script>
            $("#identification").hide();

            $(document).ready(function(){
                if ( $("#pub").is(':checked') ) $("#identification").hide();
                if ( $("#priv").is(':checked') ) $("#identification").show();
                $("input[name=type]").click(function() {
                    if ( $("#pub").is(':checked') ) $("#identification").hide();
                    if ( $("#priv").is(':checked') ) $("#identification").show();
                });
            });
        </script>
    </body>
</html>